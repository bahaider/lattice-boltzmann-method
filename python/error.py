class InvalidDirectoryError(Exception):
    def __init__(self, path):
        Exception.__init__(self,
                           "The directory '{}' does not exist.".format(path))


class EmptyArgumentError(Exception):
    def __init__(self, name):
        Exception.__init__(self,
                           "The argument `{}` cannot be empty.".format(name))
