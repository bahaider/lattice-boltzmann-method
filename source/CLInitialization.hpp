#ifndef CL_CLInitialization_hpp
#define CL_CLInitialization_hpp

#include <boost/shared_ptr.hpp>

#include <CL/cl.hpp>

namespace CL {
using namespace cl;

typedef VECTOR_CLASS<cl::Device> DeviceList;

class CLInitialization {
public:
  typedef boost::shared_ptr<CLInitialization> Shared;

public:
  virtual void
  initialize() = 0;

  virtual Context const&
  context() const = 0;

  virtual DeviceList const&
  devices() const = 0;
};   /* ----- end of CLInitialization ----- */
} /* ----- end of CL ----- */

#endif /* ----- end CL_CLInitialization_hpp ----- */
