#ifndef GLWidget_hpp
#define GLWidget_hpp

#include "ScenarioFactory.hpp"

#include <GL/glew.h>

#include <QtOpenGL/QGLWidget>

class Scenario;

class GLWidget: public QGLWidget {
  Q_OBJECT

public:
  GLWidget(ScenarioFactory::Shared scenarioFactory, QWidget* parent = 0);

  ~GLWidget();

Q_SIGNALS:
  void
  scenarioInitialized(Scenario* scenario);

private:
  void
  initializeGL();

  void
  resizeGL(int width, int height);

  void
  paintGL();

  void
  keyPressEvent(QKeyEvent* keyEvent);

  void
  keyReleaseEvent(QKeyEvent* keyEvent);

  void
  mousePressEvent(QMouseEvent* mouseEvent);

  void
  mouseReleaseEvent(QMouseEvent* mouseEvent);

  void
  mouseMoveEvent(QMouseEvent* mouseEvent);

  ScenarioFactory::Shared _scenarioFactory;
  Scenario*               _scenario;
};

#endif