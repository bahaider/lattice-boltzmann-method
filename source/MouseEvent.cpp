#include "MouseEvent.hpp"
// -----------------------------------------------------------------------------

MouseEvent::
MouseEvent(Button button,
           int    x,
           int    y,
           int    width,
           int    height,
           int    globalX,
           int    globalY,
           int    globalWidth,
           int    globalHeight):
  _button(button),
  _x(x),
  _y(y),
  _width(width),
  _height(height),
  _globalX(globalX),
  _globalY(globalY),
  _globalWidth(globalWidth),
  _globalHeight(globalHeight)
{}

Button
MouseEvent::
button() const
{ return _button; }

int
MouseEvent::
originX() const
{ return _globalX - _x; }

int
MouseEvent::
originY() const
{ return _globalY - _y; }

int
MouseEvent::
x() const
{ return _x; }

int
MouseEvent::
y() const
{ return _y; }

int
MouseEvent::
width() const
{ return _width; }

int
MouseEvent::
height() const
{ return _height; }

int
MouseEvent::
globalX() const
{ return _globalX; }

int
MouseEvent::
globalY() const
{ return _globalY; }

int
MouseEvent::
globalWidth() const
{ return _globalWidth; }

int
MouseEvent::
globalHeight() const
{ return _globalHeight; }