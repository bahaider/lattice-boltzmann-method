#ifndef Stopwatch_hpp
#define Stopwatch_hpp

#include <boost/chrono.hpp>

template <class Clock = boost::chrono::high_resolution_clock>
class Stopwatch {
public:
  inline
  Stopwatch(): _elapsed(), _running(false) {}

  inline void
  start() {
    if (_running)
      return;

    _running = true;
    _start   = Clock::now();
  }

  inline void
  stop() {
    if (!_running)
      return;

    _elapsed += Clock::now() - _start;
    _running  = false;
  }

  inline void
  reset() {
    _running = false;
    _elapsed = Clock::duration::zero();
  }

  inline void
  restart() {
    _running = true;
    _elapsed = Clock::duration::zero();
    _start   = Clock::now();
  }

  inline bool
  isRunning() const
  { return _running; }

  template <class Duration = typename Clock::duration>
  inline bool
  hasExpired(Duration const& interval) const
  { return elapsed() > interval; }

  template <class Duration = typename Clock::duration>
  inline Duration
  elapsed() const {
    using boost::chrono::duration_cast;

    return duration_cast<Duration>(_running ?
                                   _elapsed + (Clock::now() - _start) :
                                   _elapsed);
  }

private:
  typename Clock::time_point _start;
  typename Clock::duration   _elapsed;

  bool _running;
};

using HighResolutionStopwatch = Stopwatch<>;

#endif