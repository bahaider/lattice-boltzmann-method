#ifndef LbmScenarioFactory_hpp
#define LbmScenarioFactory_hpp

#include "ScenarioFactory.hpp"

class LbmScenarioFactory: public ScenarioFactory {
public:
  LbmScenarioFactory();

  LbmScenarioFactory(LbmScenarioFactory const& other);

  ~LbmScenarioFactory();

  LbmScenarioFactory const&
  operator=(LbmScenarioFactory const& other);

  Scenario*
  createScenario(QGLWidget* glWidget);

  static ScenarioFactory::Shared
  shared();
}; /* ----- end of LbmScenarioFactory ----- */

#endif /* ----- end LbmScenarioFactory_hpp ----- */
