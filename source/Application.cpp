#include "Application.hpp"

#include "Animator.hpp"
#include "Console.hpp"
#include "GLWidget.hpp"
#include "LbmScenario.hpp"
#include "LbmScenarioFactory.hpp"
#include "LbmWorker.hpp"
#include "ScenarioFactory.hpp"

#include <QtCore/QThread>

#include <QtGui/QDockWidget>
#include <QtGui/QMdiArea>
#include <QtGui/QMdiSubWindow>

#include <QtOpenGL/QGLFormat>
// -----------------------------------------------------------------------------

Application::
Application(int& argc, char** argv): QApplication(argc, argv) {
  setOrganizationName("Group #7");
  setApplicationName("Lattice-Boltzmann Method");

  QGLFormat glFormat;
#if 0
  glFormat.setAlpha(true);
#endif
  glFormat.setVersion(4, 0);
  glFormat.setProfile(QGLFormat::CoreProfile);
  QGLFormat::setDefaultFormat(glFormat);

  _console  = new Console;
  _glWidget = new GLWidget(LbmScenarioFactory::shared());

  connect(_glWidget, SIGNAL(scenarioInitialized(Scenario*)),
          this,      SLOT(initializeScenario(Scenario*)));

  _glWidget->setMinimumSize(320, 240);

  _animator = new Animator(_glWidget);
  _animator->start();

  auto dockWidget = new QDockWidget(&_mainWindow);

  dockWidget->setWidget(_console);

  _mainWindow.mdiArea()->addSubWindow(_glWidget)->showMaximized();
  _mainWindow.addDockWidget(Qt::BottomDockWidgetArea, dockWidget);
  _mainWindow.showMaximized();
}

void
Application::
initializeScenario(Scenario* scenario) {
  auto lbmScenario = static_cast<LbmScenario*>(scenario);

  lbmScenario->setPrinter(_console);
  connect(_mainWindow.runSimulationAction(), SIGNAL(triggered()),
          lbmScenario, SLOT(start()));
  connect(_mainWindow.resetSimulationAction(), SIGNAL(triggered()),
          lbmScenario, SLOT(reset()));
  connect(_mainWindow.toggleSimulationModeAction(), SIGNAL(triggered()),
          lbmScenario, SLOT(toggleSimulationMode()));
  connect(lbmScenario, SIGNAL(simulationModeChanded(int)),
          &_mainWindow, SLOT(changeSimulationMode(int)));
  lbmScenario->toggleSimulationMode();
  connect(_mainWindow.increaseDelayAction(), SIGNAL(triggered()),
          lbmScenario, SLOT(increaseDelay()));
  connect(_mainWindow.reduceDelayAction(), SIGNAL(triggered()),
          lbmScenario, SLOT(reduceDelay()));

  connect(&_mainWindow, SIGNAL(domainSizeXChanged(int)),
          lbmScenario, SLOT(domainSizeX(int)));
  lbmScenario->domainSizeX(_mainWindow.domainSizeX());

  connect(&_mainWindow, SIGNAL(domainSizeYChanged(int)),
          lbmScenario, SLOT(domainSizeY(int)));
  lbmScenario->domainSizeY(_mainWindow.domainSizeY());

  connect(&_mainWindow, SIGNAL(domainSizeZChanged(int)),
          lbmScenario, SLOT(domainSizeZ(int)));
  lbmScenario->domainSizeZ(_mainWindow.domainSizeZ());

  connect(&_mainWindow, SIGNAL(scenarioFilePathChanged(QString const &)),
          lbmScenario, SLOT(scenarioFilePath(QString const &)));
  lbmScenario->scenarioFilePath(_mainWindow.scenarioFilePath());
}
