#ifndef MouseListener_hpp
#define MouseListener_hpp

class MouseEvent;

class MouseListener {
public:
  virtual
  ~MouseListener() = 0;

  virtual void
  press(MouseEvent const& event) = 0;

  virtual void
  release(MouseEvent const& event) = 0;

  virtual void
  move(MouseEvent const& event) = 0;
};

#endif