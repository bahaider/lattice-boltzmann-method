#ifndef ScenarioFactory_hpp
#define ScenarioFactory_hpp

#include <boost/shared_ptr.hpp>

class Scenario;
class QGLWidget;

class ScenarioFactory {
public:
  typedef boost::shared_ptr<ScenarioFactory> Shared;

public:
  virtual
  ~ScenarioFactory();

  virtual Scenario*
  createScenario(QGLWidget* glWidget) = 0;
}; /* ----- end of ScenarioFactory ----- */

#endif /* ----- end ScenarioFactory_hpp ----- */
