#include "LbmScenario.hpp"

#include "KeyboardEvent.hpp"
#include "LbmWorker.hpp"
#include "MouseEvent.hpp"
#include "Printer.hpp"

#include <QtCore/QDebug>
#include <QtCore/QMetaObject>
#include <QtCore/QThread>
#include <QtOpenGL/QGLContext>

#include <QtGui/QApplication>

// -----------------------------------------------------------------------------
using Math::Matrix4x4f;

LbmScenario::
LbmScenario(QGLWidget* glWidget): _started(false),
                                  _printer(0),
                                  _thread(new QThread()),
                                  _lbmWorker(new LbmWorker()) {
  // Move LbmWorker to another thread
  _lbmWorker->moveToThread(_thread);

  // QObject::connect(_thread, SIGNAL(started()), _lbmWorker,
  //                 SLOT(setGLContext()));

  // Clean-up
  //  QObject::connect(_lbmWorker, SIGNAL(finished()), _thread, SLOT(quit()));
  //  QObject::connect(_lbmWorker, SIGNAL(finished()), _lbmWorker,
  //                   SLOT(deleteLater()));
  // QObject::connect(_thread, SIGNAL(finished()), _thread,
  //                 SLOT(deleteLater()));

  // Connect LbmWorker
  connect(_lbmWorker, SIGNAL(simulationModeChanded(int)),
          this, SLOT(emitSimulationModeChanged(int)));
  _thread->start();

  _glContext = new QGLContext(QGLContext::currentContext()->format(), glWidget);

  if (!_glContext->create(QGLContext::currentContext()))
    FATAL("Cannot create GL context");
}

void
LbmScenario::
initialize() {
  // LBM solver initialization

  QMetaObject::invokeMethod(_lbmWorker,
                            "initialize",
                            Qt::BlockingQueuedConnection,
                            Q_ARG(QGLContext*, _glContext));

  initializeRenderers();
}

void
LbmScenario::
initializeRenderers() {
  densityField = new ScalarField(_lbmWorker->sizeX(),
                                 _lbmWorker->sizeY(),
                                 _lbmWorker->sizeZ());
  velocityField = new VectorField(_lbmWorker->sizeX(),
                                  _lbmWorker->sizeY(),
                                  _lbmWorker->sizeZ());
  flagField = new FlagField(_lbmWorker->sizeX(),
                            _lbmWorker->sizeY(),
                            _lbmWorker->sizeZ());
  densityRenderer = new DensityRenderer(densityField);
  domainRenderer  = new DomainRenderer(_lbmWorker->sizeX(),
                                       _lbmWorker->sizeY(),
                                       _lbmWorker->sizeZ());
  particleRenderer = new ParticleRenderer(velocityField);
  velocityRenderer = new VelocityRenderer(velocityField);

  densityField->attach();
  _lbmWorker->setDensityBuffer(GL_TEXTURE_3D,
                               densityField->expose());
  densityField->detach();

  velocityField->attach();
  _lbmWorker->setVelocityBuffer(GL_TEXTURE_3D,
                                velocityField->expose());
  velocityField->detach();
  _lbmWorker->updateSharedBuffers();

  flagField->attach();
  _lbmWorker->setFlagBuffer(GL_TEXTURE_3D,
                            flagField->expose());
  flagField->detach();
  _lbmWorker->updateFlagBuffer();

  gl.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  gl.ClearDepth(1);
  // gl.PolygonMode(PolygonMode::Line);
}

void
LbmScenario::
reset() {
  // _thread->quit();
  // QMetaObject::invokeMethod(_lbmWorker, "deleteLater");
  _lbmWorker->reset();
  delete densityField;
  delete velocityField;
  delete flagField;
  delete densityRenderer;
  delete domainRenderer;
  delete particleRenderer;
  delete velocityRenderer;
  initializeRenderers();
}

void
LbmScenario::
setPrinter(Printer* printer)
{ _printer = printer; }

Printer*
LbmScenario::
printer() const
{ return _printer; }

void
LbmScenario::
start() {
  QMetaObject::invokeMethod(_lbmWorker, "start");
  _started = true;
}

void
LbmScenario::
toggleSimulationMode() {
  QMetaObject::invokeMethod(_lbmWorker,
                            "toggleSimulationMode");
}

void
LbmScenario::
emitSimulationModeChanged(int mode) {
  emit simulationModeChanded(mode);
}

void
LbmScenario::
increaseDelay() {
  QMetaObject::invokeMethod(_lbmWorker,
                            "increaseDelay");
}

void
LbmScenario::
reduceDelay() {
  QMetaObject::invokeMethod(_lbmWorker,
                            "reduceDelay");
}

void
LbmScenario::
domainSizeX(int sizeX) {
  QMetaObject::invokeMethod(_lbmWorker,
                            "sizeX",
                            Qt::BlockingQueuedConnection,
                            Q_ARG(int, sizeX));
}

void
LbmScenario::
domainSizeY(int sizeY) {
  QMetaObject::invokeMethod(_lbmWorker,
                            "sizeY",
                            Qt::BlockingQueuedConnection,
                            Q_ARG(int, sizeY));
}

void
LbmScenario::
domainSizeZ(int sizeZ) {
  QMetaObject::invokeMethod(_lbmWorker,
                            "sizeZ",
                            Qt::BlockingQueuedConnection,
                            Q_ARG(int, sizeZ));
}

void
LbmScenario::
scenarioFilePath(QString scenaioFilePath) {
  _lbmWorker->scenarioFilePath(scenaioFilePath);
}

void
LbmScenario::
resize(int width, int height) {
  gl.Viewport(0, 0, width, height);

  projection = camera.projection(width, height);
}

void
LbmScenario::
render() {
  using seconds = boost::chrono::duration<float>;

  _iterationCounter.update();

  auto timeStep = stopwatch.elapsed<seconds>().count();
  stopwatch.restart();

  camera.move(10 * movementManager.movement<float>());
  camera.yaw(eulerRotationManager.yaw());
  camera.pitch(eulerRotationManager.pitch());

  view = camera.view();

  Matrix4x4f vp = projection * view;

  // ---------------------------------------------------------------------------

  gl.Clear().ColorBuffer().DepthBuffer();

#if 1

  if (_started) {
    densityRenderer->render(vp, 0.5f);
    particleRenderer->render(vp, timeStep, 100, 10);
    velocityRenderer->render(vp, 8);
    _lbmWorker->updateSharedBuffers();
  }

  domainRenderer->render(vp, 1, 1, 1);

#endif

  _printer->print(QString("%1").arg(_iterationCounter.iterationsPerSecond()));
}

void
LbmScenario::
press(KeyboardEvent const& event) {
  switch (event.key()) {
  case Key::a:
    movementManager.startLeft();
    break;

  case Key::d:
    movementManager.startRight();
    break;

  case Key::e:
    movementManager.startUp();
    break;

  case Key::q:
    movementManager.startDown();
    break;

  case Key::w:
    movementManager.startForward();
    break;

  case Key::s:
    movementManager.startBackward();
    break;
  }
}

void
LbmScenario::
release(KeyboardEvent const& event) {
  switch (event.key()) {
  case Key::a:
    movementManager.stopLeft();
    break;

  case Key::d:
    movementManager.stopRight();
    break;

  case Key::e:
    movementManager.stopUp();
    break;

  case Key::q:
    movementManager.stopDown();
    break;

  case Key::w:
    movementManager.stopForward();
    break;

  case Key::s:
    movementManager.stopBackward();
    break;
  }
}

void
LbmScenario::
press(MouseEvent const& event) {
  switch (event.button()) {
  case Button::left:
    eulerRotationManager.start();
    // QApplication::setOverrideCursor(QCursor(Qt::BlankCursor));
    QCursor::setPos(event.originX() + event.width()  / 2,
                    event.originY() + event.height() / 2);
    break;
  }
}

void
LbmScenario::
release(MouseEvent const& event) {
  switch (event.button()) {
  case Button::left:
    eulerRotationManager.stop();
    // QApplication::setOverrideCursor(QCursor());
    break;
  }
}

void
LbmScenario::
move(MouseEvent const& event) {
  eulerRotationManager.advance(event.x(),
                               event.y(),
                               event.width(),
                               event.height(),
                               camera.fovY(),
                               camera.nearZ());

  QCursor::setPos(event.originX() + event.width()  / 2,
                  event.originY() + event.height() / 2);
}
