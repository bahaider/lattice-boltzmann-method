#ifndef Vector_hpp
#define Vector_hpp

#include <Eigen/Geometry>

namespace Math {
template <class T, int size>
using Vector = Eigen::Matrix<T, size, 1>;

template <class T>
using Vector3 = Vector<T, 3>;

template <class T>
using Vector4 = Vector<T, 4>;

using Vector3f = Vector<float, 3>;
using Vector4f = Vector<float, 4>;
}

#endif