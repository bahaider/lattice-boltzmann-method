#include "MainWindow.hpp"

#include <QtCore/QCoreApplication>
#include <QtGui/QAction>
#include <QtGui/QCloseEvent>
#include <QtGui/QFileDialog>
#include <QtGui/QLineEdit>
#include <QtGui/QMdiArea>
#include <QtGui/QMessageBox>
#include <QtGui/QSpinBox>
#include <QtGui/QToolBar>

#include <QtCore/QDebug>
//
// -----------------------------------------------------------------------------

MainWindow::
MainWindow() {
  _mdiArea = new QMdiArea(this);

  setCentralWidget(_mdiArea);
  setDockOptions(AnimatedDocks | AllowNestedDocks | AllowTabbedDocks);
  createActions();
  createToolBars();
}

void
MainWindow::
createActions() {
  _runSimulationAction = new QAction(QIcon(":/image/run.png"),
                                     tr("Run Simulation"), this);

  _resetSimulationAction = new QAction(QIcon(":/image/reset.png"),
                                       tr("Reset Simulation"), this);

  _toggleSimulationMode = new QAction(QIcon(":/image/autoRescale.png"),
                                      tr("Toggle Simultion Mode"), this);

  _increaseDelayAction = new QAction(QIcon(":/image/increaseDelay.png"),
                                     tr("Increase Speed"), this);

  _reduceDelayAction = new QAction(QIcon(":/image/reduceDelay.png"),
                                   tr("Reduce Speed"), this);

  _domainSizeX = new QSpinBox();
  _domainSizeX->setValue(20);
  connect(_domainSizeX, SIGNAL(valueChanged(int)),
          this, SIGNAL(domainSizeXChanged(int)));
  _domainSizeY = new QSpinBox();
  _domainSizeY->setValue(20);
  connect(_domainSizeY, SIGNAL(valueChanged(int)),
          this, SIGNAL(domainSizeYChanged(int)));
  _domainSizeZ = new QSpinBox();
  _domainSizeZ->setValue(20);
  connect(_domainSizeZ, SIGNAL(valueChanged(int)),
          this, SIGNAL(domainSizeZChanged(int)));

  _chooseFileAction = new QAction(QIcon(":/image/open.png"),
                                  tr("Open Scenario"), this);
  connect(_chooseFileAction, SIGNAL(triggered()),
          this, SLOT(changeScenarioFilePath()));

  _scenarioFilePathLineEdit =
    new QLineEdit(QString("%1/scenario1.cl")
                  .arg(QCoreApplication::applicationDirPath()));
  connect(_scenarioFilePathLineEdit, SIGNAL(textChanged(QString)),
          this, SIGNAL(scenarioFilePathChanged(QString)));
}

void
MainWindow::
createToolBars() {
  _toolBar = addToolBar(tr("Main Tool Bar"));
  _toolBar->addAction(_runSimulationAction);
  _toolBar->addAction(_resetSimulationAction);
  _toolBar->addAction(_toggleSimulationMode);
  _toolBar->addAction(_increaseDelayAction);
  _toolBar->addAction(_reduceDelayAction);

  _toolBar->addWidget(_domainSizeX);
  _toolBar->addWidget(_domainSizeY);
  _toolBar->addWidget(_domainSizeZ);

  _toolBar->addAction(_chooseFileAction);
  _toolBar->addWidget(_scenarioFilePathLineEdit);
}

QMdiArea*
MainWindow::
mdiArea() const
{ return _mdiArea; }

QAction*
MainWindow::
runSimulationAction() const
{ return _runSimulationAction; }

QAction*
MainWindow::
resetSimulationAction() const
{ return _resetSimulationAction; }

QAction*
MainWindow::
toggleSimulationModeAction() const
{ return _toggleSimulationMode; }

void
MainWindow::
changeSimulationMode(int mode) {
  if (mode == 0)
    _toggleSimulationMode->setIcon(QIcon(":/image/autoRescale.png"));
  else
    _toggleSimulationMode->setIcon(QIcon(":/image/rescale.png"));
}

QAction*
MainWindow::
increaseDelayAction() const
{ return _increaseDelayAction; }

QAction*
MainWindow::
reduceDelayAction() const
{ return _reduceDelayAction; }

int
MainWindow::
domainSizeX() const
{ return _domainSizeX->value(); }

int
MainWindow::
domainSizeY() const
{ return _domainSizeY->value(); }

int
MainWindow::
domainSizeZ() const
{ return _domainSizeZ->value(); }

void
MainWindow::
changeScenarioFilePath() {
  QString fileName =
    QFileDialog::getOpenFileName(this,
                                 tr("Open Scenario"),
                                 QCoreApplication::applicationDirPath(),
                                 tr("OpenCL Files (*.cl)"));
  _scenarioFilePathLineEdit->setText(fileName);
}

QString const&
MainWindow::
scenarioFilePath() const
{ return _scenarioFilePathLineEdit->text(); }

void
MainWindow::
closeEvent(QCloseEvent* event) {
  QMessageBox::StandardButton button =
    QMessageBox::warning(this,
                         "Quit Confirmation",
                         "Are you sure you want to quit?",
                         QMessageBox::Yes | QMessageBox::No,
                         QMessageBox::No);

  if (button == QMessageBox::Yes)
    event->accept();
  else
    event->ignore();
}
