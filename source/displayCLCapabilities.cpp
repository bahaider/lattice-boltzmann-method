#include "displayCLCapabilities.hpp"

#include <CL/cl.hpp>

#include <boost/lexical_cast.hpp>

#include <iostream>
#include <sstream>
#include <string>

using cl::Device;
using cl::Platform;

using boost::lexical_cast;

using std::cout;
using std::endl;
using std::ostringstream;
using std::size_t;
using std::string;
using std::vector;

template <typename Target, typename Source>
Target
printArray(const Source*  arg,
           const::size_t& size) {
  Target result;

  for (size_t i = 0; i < size; ++i) {
    if (i != 0)
      result += ", ";

    result += boost::lexical_cast<Target>(arg[i]);
  }

  return result;
}

void
CL::
displayCLCapabilities() {
  vector<Platform> platforms;
  Platform::get(&platforms);
  ostringstream oss;

  for (int i = 0; i < platforms.size(); ++i) {
    string                      s;
    cl_uint                     uInt;
    cl_ulong                    uLong;
    cl_bool                     bl;
    cl_device_type              devType;
    cl_device_fp_config         devFpConfig;
    cl_device_mem_cache_type    devMemCacheType;
    cl_device_local_mem_type    devLocalMemType;
    cl_device_exec_capabilities devExecCap;
    cl_command_queue_properties devCommandQueueProps;
    cl_platform_id              devPlatform;
    size_t                      st;
    vector<size_t>              stVector;

    s = platforms[i].getInfo<CL_PLATFORM_NAME>();
    oss << "  " << "CL_PLATFORM_NAME: " << s << endl;

    s = platforms[i].getInfo<CL_PLATFORM_PROFILE>();
    oss << "  " << "CL_PLATFORM_PROFILE: " << s << endl;

    s = platforms[i].getInfo<CL_PLATFORM_VERSION>();
    oss << "  " << "CL_PLATFORM_VERSION: " << s << endl;

    s = platforms[i].getInfo<CL_PLATFORM_VENDOR>();
    oss << "  " << "CL_PLATFORM_VENDOR: " << s << endl;

    s = platforms[i].getInfo<CL_PLATFORM_EXTENSIONS>();
    oss << "  " << "CL_PLATFORM_EXTENSIONS: " << s << endl;

    vector<Device> devices;
    platforms[i].getDevices(CL_DEVICE_TYPE_ALL, &devices);

    for (int j = 0; j < devices.size(); ++j) {
      devType = devices[j].getInfo<CL_DEVICE_TYPE>();
      oss << "    " << "CL_DEVICE_TYPE: " <<
        lexical_cast<string>(devType) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_VENDOR_ID>();
      oss << "    " << "CL_DEVICE_VENDOR_ID: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();
      oss << "    " << "CL_DEVICE_MAX_COMPUTE_UNITS: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS>();
      oss << "    " << "CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS: " <<
        lexical_cast<string>(uInt) << endl;

      st = devices[j].getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
      oss << "    " << "CL_DEVICE_MAX_WORK_GROUP_SIZE: " <<
        lexical_cast<string>(st) << endl;

      stVector = devices[j].getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>();
      oss << "    " << "CL_DEVICE_MAX_WORK_ITEM_SIZES: " <<
        printArray<string>(&*stVector.begin(), stVector.size()) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR>();
      oss << "    " << "CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT>();
      oss << "    " << "CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT>();
      oss << "    " << "CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG>();
      oss << "    " << "CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT>();
      oss << "    " << "CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE>();
      oss << "    " << "CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>();
      oss << "    " << "CL_DEVICE_MAX_CLOCK_FREQUENCY: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_ADDRESS_BITS>();
      oss << "    " << "CL_DEVICE_ADDRESS_BITS: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_MAX_READ_IMAGE_ARGS>();
      oss << "    " << "CL_DEVICE_MAX_READ_IMAGE_ARGS: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_MAX_WRITE_IMAGE_ARGS>();
      oss << "    " << "CL_DEVICE_MAX_WRITE_IMAGE_ARGS: " <<
        lexical_cast<string>(uInt) << endl;

      uLong = devices[j].getInfo<CL_DEVICE_MAX_MEM_ALLOC_SIZE>();
      oss << "    " << "CL_DEVICE_MAX_MEM_ALLOC_SIZE: " <<
        lexical_cast<string>(uLong) << endl;

      st = devices[j].getInfo<CL_DEVICE_IMAGE2D_MAX_WIDTH>();
      oss << "    " << "CL_DEVICE_IMAGE2D_MAX_WIDTH: " <<
        lexical_cast<string>(st) << endl;

      st = devices[j].getInfo<CL_DEVICE_IMAGE2D_MAX_HEIGHT>();
      oss << "    " << "CL_DEVICE_IMAGE2D_MAX_HEIGHT: " <<
        lexical_cast<string>(st) << endl;

      st = devices[j].getInfo<CL_DEVICE_IMAGE3D_MAX_WIDTH>();
      oss << "    " << "CL_DEVICE_IMAGE3D_MAX_WIDTH: " <<
        lexical_cast<string>(st) << endl;

      st = devices[j].getInfo<CL_DEVICE_IMAGE3D_MAX_HEIGHT>();
      oss << "    " << "CL_DEVICE_IMAGE3D_MAX_HEIGHT: " <<
        lexical_cast<string>(st) << endl;

      st = devices[j].getInfo<CL_DEVICE_IMAGE3D_MAX_DEPTH>();
      oss << "    " << "CL_DEVICE_IMAGE3D_MAX_DEPTH: " <<
        lexical_cast<string>(st) << endl;

      bl = devices[j].getInfo<CL_DEVICE_IMAGE_SUPPORT>();
      oss << "    " << "CL_DEVICE_IMAGE_SUPPORT: " <<
        lexical_cast<string>(bl) << endl;

      st = devices[j].getInfo<CL_DEVICE_MAX_PARAMETER_SIZE>();
      oss << "    " << "CL_DEVICE_MAX_PARAMETER_SIZE: " <<
        lexical_cast<string>(st) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_MAX_SAMPLERS>();
      oss << "    " << "CL_DEVICE_MAX_SAMPLERS: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_MEM_BASE_ADDR_ALIGN>();
      oss << "    " << "CL_DEVICE_MEM_BASE_ADDR_ALIGN: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE>();
      oss << "    " << "CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE: " <<
        lexical_cast<string>(uInt) << endl;

      devFpConfig = devices[j].getInfo<CL_DEVICE_SINGLE_FP_CONFIG>();
      oss << "    " << "CL_DEVICE_SINGLE_FP_CONFIG: " <<
        lexical_cast<string>(devFpConfig) << endl;

      devMemCacheType = devices[j].getInfo<CL_DEVICE_GLOBAL_MEM_CACHE_TYPE>();
      oss << "    " << "CL_DEVICE_GLOBAL_MEM_CACHE_TYPE: " <<
        lexical_cast<string>(devMemCacheType) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE>();
      oss << "    " << "CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE: " <<
        lexical_cast<string>(uInt) << endl;

      uLong = devices[j].getInfo<CL_DEVICE_GLOBAL_MEM_CACHE_SIZE>();
      oss << "    " << "CL_DEVICE_GLOBAL_MEM_CACHE_SIZE: " <<
        lexical_cast<string>(uLong) << endl;

      uLong = devices[j].getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>();
      oss << "    " << "CL_DEVICE_GLOBAL_MEM_SIZE: " <<
        lexical_cast<string>(uLong) << endl;

      uLong = devices[j].getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>();
      oss << "    " << "CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE: " <<
        lexical_cast<string>(uLong) << endl;

      uLong = devices[j].getInfo<CL_DEVICE_MAX_CONSTANT_ARGS>();
      oss << "    " << "CL_DEVICE_MAX_CONSTANT_ARGS: " <<
        lexical_cast<string>(uLong) << endl;

      devLocalMemType = devices[j].getInfo<CL_DEVICE_LOCAL_MEM_TYPE>();
      oss << "    " << "CL_DEVICE_LOCAL_MEM_TYPE: " <<
        lexical_cast<string>(devLocalMemType) << endl;

      uLong = devices[j].getInfo<CL_DEVICE_LOCAL_MEM_SIZE>();
      oss << "    " << "CL_DEVICE_LOCAL_MEM_SIZE: " <<
        lexical_cast<string>(uLong) << endl;

      bl = devices[j].getInfo<CL_DEVICE_ERROR_CORRECTION_SUPPORT>();
      oss << "    " << "CL_DEVICE_ERROR_CORRECTION_SUPPORT: " <<
        lexical_cast<string>(bl) << endl;

      st = devices[j].getInfo<CL_DEVICE_PROFILING_TIMER_RESOLUTION>();
      oss << "    " << "CL_DEVICE_PROFILING_TIMER_RESOLUTION: " <<
        lexical_cast<string>(st) << endl;

      bl = devices[j].getInfo<CL_DEVICE_ENDIAN_LITTLE>();
      oss << "    " << "CL_DEVICE_ENDIAN_LITTLE: " <<
        lexical_cast<string>(bl) << endl;

      bl = devices[j].getInfo<CL_DEVICE_AVAILABLE>();
      oss << "    " << "CL_DEVICE_AVAILABLE: " <<
        lexical_cast<string>(bl) << endl;

      bl = devices[j].getInfo<CL_DEVICE_COMPILER_AVAILABLE>();
      oss << "    " << "CL_DEVICE_COMPILER_AVAILABLE: " <<
        lexical_cast<string>(bl) << endl;

      devExecCap = devices[j].getInfo<CL_DEVICE_EXECUTION_CAPABILITIES>();
      oss << "    " << "CL_DEVICE_EXECUTION_CAPABILITIES: " <<
        lexical_cast<string>(devExecCap) << endl;

      devCommandQueueProps = devices[j].getInfo<CL_DEVICE_QUEUE_PROPERTIES>();
      oss << "    " << "CL_DEVICE_QUEUE_PROPERTIES: " <<
        lexical_cast<string>(devCommandQueueProps) << endl;

      bl = devices[j].getInfo<CL_DEVICE_HOST_UNIFIED_MEMORY>();
      oss << "    " << "CL_DEVICE_HOST_UNIFIED_MEMORY: " <<
        lexical_cast<string>(bl) << endl;

      s = devices[j].getInfo<CL_DEVICE_NAME>();
      oss << "    " << "CL_DEVICE_NAME: " << s << endl;

      s = devices[j].getInfo<CL_DEVICE_VENDOR>();
      oss << "    " << "CL_DEVICE_VENDOR: " << s << endl;

      s = devices[j].getInfo<CL_DRIVER_VERSION>();
      oss << "    " << "CL_DRIVER_VERSION: " << s << endl;

      s = devices[j].getInfo<CL_DEVICE_PROFILE>();
      oss << "    " << "CL_DEVICE_PROFILE: " << s << endl;

      s = devices[j].getInfo<CL_DEVICE_VERSION>();
      oss << "    " << "CL_DEVICE_VERSION: " << s << endl;

      s = devices[j].getInfo<CL_DEVICE_EXTENSIONS>();
      oss << "    " << "CL_DEVICE_EXTENSIONS: " << s << endl;

      devPlatform = devices[j].getInfo<CL_DEVICE_PLATFORM>();
      oss << "    " << "CL_DEVICE_PLATFORM: " <<
        lexical_cast<string>(devPlatform) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF>();
      oss << "    " << "CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR>();
      oss << "    " << "CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT>();
      oss << "    " << "CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_INT>();
      oss << "    " << "CL_DEVICE_NATIVE_VECTOR_WIDTH_INT: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG>();
      oss << "    " << "CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT>();
      oss << "    " << "CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE>();
      oss << "    " << "CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE: " <<
        lexical_cast<string>(uInt) << endl;

      uInt = devices[j].getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF>();
      oss << "    " << "CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF: " <<
        lexical_cast<string>(uInt) << endl;
    }

    cout
      << "----------------------------------------"
      << "----------------------------------------" << endl
      << "OpenCL Capablilities:" << endl
      << oss.str()
      << "----------------------------------------"
      << "----------------------------------------" << endl;
  }
}