#ifndef ParticleRenderer_hpp
#define ParticleRenderer_hpp

#include "Matrix.hpp"

#include <GL/glew.h>

#include <oglplus/all.hpp>

class VectorField;

class ParticleRenderer {
public:
  ParticleRenderer(VectorField const* velocityField);

  GLuint
  sampleCountX() const;

  GLuint
  sampleCountY() const;

  GLuint
  sampleCountZ() const;

  void
  render(Math::Matrix4x4f const& mvp,
         GLfloat                 timeStep,
         GLfloat                 amplifier,
         GLfloat                 size) const;

private:
  void
  initializeProgram();

  void
  initializeParticles();

  oglplus::Context _gl;

  oglplus::Program _program;

  oglplus::VertexArray _vertexArray[2];

  oglplus::Buffer _positionBuffer[2];

  oglplus::TransformFeedback _transformFeedback[2];

  GLuint _subroutineUpdate;
  GLuint _subroutineRender;

  GLuint _particleCount;

  GLuint _index;

  VectorField const* _velocityField;
};

#endif