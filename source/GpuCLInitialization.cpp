#include "GpuCLInitialization.hpp"

#if defined (__MINGW32__)
  #include <windows.h>
#elif defined (__GNUC__)
  #include <GL/glx.h>
#else
  #error ("The platform is not supported by the application")
#endif

#include "displayCLCapabilities.hpp"

#include "LoggingMacros.hpp"
// ---

#if defined (__MINGW32__)
void
getContextProperties(CL::Platform const&    platform,
                     cl_context_properties* cps) {
  // Select the default platform and create
  // a context using this platform and the GPU
  cps[0] = CL_GL_CONTEXT_KHR;
  cps[1] = (cl_context_properties)wglGetCurrentContext();
  cps[2] = CL_WGL_HDC_KHR;
  cps[3] = (cl_context_properties)wglGetCurrentDC();
  cps[4] = CL_CONTEXT_PLATFORM;
  cps[5] = (cl_context_properties)(platform());
  cps[6] = 0;
}

#elif defined (__GNUC__)

void
getContextProperties(CL::Platform const&    platform,
                     cl_context_properties* cps) {
  // Select the default platform and create
  // a context using this platform and the GPU
  cps[0] = CL_GL_CONTEXT_KHR;
  cps[1] = (cl_context_properties)glXGetCurrentContext();
  cps[2] = CL_WGL_HDC_KHR;
  cps[3] = (cl_context_properties)glXGetCurrentDisplay();
  cps[4] = CL_CONTEXT_PLATFORM;
  cps[5] = (cl_context_properties)(platform());
  cps[6] = 0;
}

#else
  #error ("The platform is not supported by the application")
#endif
// ---

GpuCLInitialization::
GpuCLInitialization() {}

GpuCLInitialization::
GpuCLInitialization(GpuCLInitialization const& other) {}

GpuCLInitialization::~GpuCLInitialization()
{}

GpuCLInitialization const&
GpuCLInitialization::
operator=(GpuCLInitialization const& other) {}

void
GpuCLInitialization::
initialize() {
  try {
    CL::displayCLCapabilities();

    // Get available platforms
    std::vector<cl::Platform> platforms;
    CL::Platform::get(&platforms);
    cl_context_properties cps[7];
    getContextProperties(platforms[0], cps);

    _context = cl::Context(CL_DEVICE_TYPE_GPU, cps);

    _devices = _context.getInfo<CL_CONTEXT_DEVICES>();
  } catch (cl::Error const& exc) {
    FATAL("An error occured during OpenCL initialization at '%s': %i",
          exc.what(), exc.err());
  }
}

CL::Context const&
GpuCLInitialization::
context() const {
  return _context;
}

CL::DeviceList const&
GpuCLInitialization::
devices() const {
  return _devices;
}
